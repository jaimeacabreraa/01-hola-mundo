import { Component } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: 'body.component.html',
})

export class BodyComponent {




  estado: boolean = true
  lista: string[] = [
    "Jaime",
    "Juan",
    "Carlos",
    "Pedro"
  ]

  frase:any={
    mensaje:"Un gran poder requiere una gran responsabilidad",
    autor:"Peter Parker"
  }

  constructor() {

  }

  mostrar() {
    this.estado = !this.estado
  }

}
