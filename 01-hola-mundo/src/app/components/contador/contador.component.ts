import { Component } from '@angular/core';

@Component({
  selector: 'app-contador',
  templateUrl: 'contador.component.html',
})

export class ContadorComponent {
  numero: number = 55;
  base:number=5;

  restar() {
    this.numero -= 1;
    if (this.numero <= 0) {
      this.numero = 0;
    }
  }

  sumar() {
    this.numero += 1;
  }

  acumular(valor:number){
    this.numero+=valor
  }

  /*sumar = () => {
    this.numero += 1;
    console.log(this.numero);
  }

  restar = () => {
    this.numero -= 1;
    console.log(this.numero);
  }*/
}
